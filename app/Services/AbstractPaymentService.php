<?php

namespace App\Services;

use App\Exceptions\UnauthorizedException;
use App\Models\Transaction;
use App\Repositories\TransactionRepositoryInterface;
use League\Config\Exception\InvalidConfigurationException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

abstract class AbstractPaymentService
{
    protected int $limit;
    public const STATUS_CREATED = 0;
    public const STATUS_PROCESSING = 1;
    public const STATUS_PAID = 2;
    public const STATUS_EXPIRED = 3;
    public const STATUS_REJECTED = 4;

    public TransactionRepositoryInterface $transactionRepository;


    public function __construct()
    {
        $this->transactionRepository = app(TransactionRepositoryInterface::class);

        $this->limit = config('payment.' . $this->getMerchantType() . '.limit_transaction_per_day');

        if (!ctype_digit($this->limit)) {
            throw new InvalidConfigurationException();
        }

        if ($this->limit < -1) {
            throw new InvalidConfigurationException();
        }
    }

    /**
     * Creates Hash and checks it
     * @param array $params
     * @return bool
     */
    abstract protected function validateSignature(array $params): bool;

    abstract protected function getExternalPaymentIdKey(): string;

    abstract protected function convertExternalStatusToTransactionStatus(string $status): int;

    abstract protected function getTransactionLimitPerDay(): int;

    abstract protected function getMerchantType(): string;

    /**
     * @param array $params
     * @return void
     * @throws UnauthorizedException
     * @throws TooManyRequestsHttpException
     * @throws InvalidConfigurationException
     */
    public function callback(array $params): void
    {
        if ($this->validateSignature($params)) {
            throw new UnauthorizedException();
        }

        if ($this->isLimitExceeded()) {
            throw new TooManyRequestsHttpException();
        }

        $transaction = $this->getTransactionById(
            $params[$this->getExternalPaymentIdKey()]
        );

        $this->setTransactionState($transaction, $this->convertExternalStatusToTransactionStatus());
    }

    private function getTransactionById($id): Transaction
    {
        return $this->transactionRepository->getByExternalId($id, $this->getMerchantType());
    }

    /**
     * @return bool
     * @throws InvalidConfigurationException
     */
    private function isLimitExceeded(): bool
    {
        if ($this->getTransactionLimitPerDay() === -1) {
            return false;
        }

        if ($this->getTransactionLimitPerDay() < 0) {
            throw new InvalidConfigurationException();
        }

        if ($this->getTransactionLimitPerDay() < $this->getTodaysTransactionCount()) {
            return true;
        }

        return false;
    }

    private function getTodaysTransactionCount(): int
    {
        return $this->transactionRepository->getTodaysTransactionCount($this->getMerchantType());
    }

    private function setTransactionState(Transaction $transaction, int $state): void
    {
        $this->transactionRepository->setStatus($transaction, $state);
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_CREATED,
            self::STATUS_PROCESSING,
            self::STATUS_PAID,
            self::STATUS_EXPIRED,
            self::STATUS_REJECTED,
        ];
    }
}