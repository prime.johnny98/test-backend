<?php

namespace App\Services;

use League\Config\Exception\InvalidConfigurationException;

class PaymentAService extends AbstractPaymentService
{
    protected string $key;
    protected string $secret;

    public function __construct()
    {
        $this->key = config('payment.a.key');
        $this->secret = config('payment.a.secret');

        parent::__construct();
    }

    protected function validateSignature(array $params): bool
    {
        $sign = $params['sign'];

        return $sign === $this->generateSign($params);
    }

    private function generateSign($params): string
    {
        ksort($params);

        unset($params['sign']);

        return hash('sha256', implode(':', $params) . $this->secret);
    }

    protected function getExternalPaymentIdKey(): string
    {
        return 'payment_id';
    }

    protected function convertExternalStatusToTransactionStatus(string $status): int
    {
        $statuses = [
            'new' => self::STATUS_CREATED,
            'pending' => self::STATUS_PROCESSING,
            'completed' => self::STATUS_PAID,
            'expired' => self::STATUS_EXPIRED,
            'rejected' => self::STATUS_REJECTED,
        ];

        return $statuses[$status];
    }

    protected function getMerchantType(): string
    {
        return 'a';
    }

    protected function getTransactionLimitPerDay(): int
    {
        return $this->limit;
    }
}