<?php

namespace App\Services;

class PaymentBService extends AbstractPaymentService {
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->key = config('payment.b.key');
        $this->secret = config('payment.b.secret');
    }

    protected function validateSignature(array $params): bool
    {
        $sign = $params['sign'];

        return $sign === $this->generateSign($params);
    }

    protected function generateSign(array $params): bool
    {
        ksort($params);

        unset($params['sign']);

        return md5(implode('.', $params) . $this->secret);
    }

    protected function processPayment()
    {
        // TODO: Implement processPayment() method.
    }

    protected function castParamsToTableInsertion(array $params): bool
    {
        return false;
    }

    protected function getExternalPaymentIdKey(): string
    {
        return 'invoice';
    }

    protected function convertExternalStatusToTransactionStatus(string $status): int
    {
        $statuses = [
            'created' => self::STATUS_CREATED,
            'inprogress' => self::STATUS_PROCESSING,
            'paid' => self::STATUS_PAID,
            'expired' => self::STATUS_EXPIRED,
            'rejected' => self::STATUS_REJECTED,
        ];

        return $statuses[$status];
    }

    protected function getMerchantType(): string
    {
        return 'b';
    }
}