<?php

namespace App\Repositories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface TransactionRepositoryInterface {
    /**
     * @param mixed $id
     * @param string $merchanType
     * @return Transaction
     * @throws ModelNotFoundException
     */
    public function getByExternalId(mixed $id, string $merchantType): Transaction;

    /**
     * @param Transaction $transaction
     * @param int $state
     * @return bool
     */
    public function setStatus(Transaction $transaction, int $state): bool;

    public function getTodaysTransactionCount(string $merchantType): int;
}