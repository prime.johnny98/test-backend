<?php

namespace App\Repositories;

use App\Models\Transaction;

class TransactionRepository implements TransactionRepositoryInterface
{
    public function getByExternalId($id, $merchantType): Transaction
    {
        return Transaction::query()
            ->where('external_id', $id)
            ->where('merchant_type', $merchantType)
            ->firstOrFail();
    }

    public function setStatus(Transaction $transaction, int $state): bool
    {
        return $transaction->update(['status' => $state]);
    }

    public function getTodaysTransactionCount(string $merchantType): int
    {
        return Transaction::query()
            ->where('merchant_type', $merchantType)
            ->today()
            ->count();
    }
}
