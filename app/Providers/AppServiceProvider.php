<?php

namespace App\Providers;

use App\Repositories\TransactionRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->registerRepositories();
    }

    private function registerRepositories(): void
    {
        $this->app->bind(TransactionRepositoryInterface::class, TransactionRepository::class);
    }
}
