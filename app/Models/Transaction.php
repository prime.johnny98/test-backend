<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'merchant_type',
        'merchant_id',
        'external_id',
        'status',
        'amount',
        'amount_paid',
    ];

    public function scopeToday(Builder $query): void
    {
        $query->whereDate(self::CREATED_AT, Carbon::today());
    }
}
