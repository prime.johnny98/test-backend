<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentACallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'merchant_id' => 'required|integer',
            'payment_id' => 'required|integer',
            'status' => 'required|in:',
            'amount' => 'integer|min:0',
            'amount_paid' => 'integer|min:0',
            'timestamp' => 'required',
            'sign' => 'required|string',
        ];
    }
}
