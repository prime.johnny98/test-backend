<?php

namespace App\Http\Controllers\Payment;

use App\Exceptions\BaseException;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentBCallbackRequest;
use App\Services\AbstractPaymentService;
use App\Services\PaymentBService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentBController extends Controller
{
    private const TOKEN_HEADER_NAME = 'Authorization';
    protected AbstractPaymentService $paymentService;

    public function __construct()
    {
        $this->paymentService = new PaymentBService();
    }

    public function callback(PaymentBCallbackRequest $request)
    {
        if (!$request->hasHeader(self::TOKEN_HEADER_NAME)) {
            return response()
                ->json([
                    'error' => 'Header "Authorization" is Required',
                    'error_code' => 'BAD_REQUEST'
                ], 400);
        }

        try {
            $this->paymentService->callback([
                ...$request->safe()->all(),
                'sign' => $request->header(self::TOKEN_HEADER_NAME),
            ]);

            return response()
                ->json([
                    'success' => true,
                ]);
        } catch (BaseException $baseException) {
            return response()
                ->json([
                    'error' => $baseException->getMessage(),
                    'error_code' => $baseException->getErrorCode(),
                ], $baseException->getCode());
        } catch (ModelNotFoundException $modelNotFoundException) {
            return response()
                ->json([
                    'error' => 'transaction not found',
                    'error_code' => 'NOT_FOUND',
                ], 404);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'error' => 'Unahendled exception',
                    'error_code' => 'UNHANDLED_EXCEPTION',
                ], 500);
        }
    }
}
