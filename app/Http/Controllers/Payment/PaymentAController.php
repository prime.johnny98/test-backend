<?php

namespace App\Http\Controllers\Payment;

use App\Exceptions\BaseException;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentACallbackRequest;
use App\Services\AbstractPaymentService;
use App\Services\PaymentAService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentAController extends Controller
{
    protected AbstractPaymentService $paymentService;

    public function __construct()
    {
        $this->paymentService = new PaymentAService();
    }

    public function callback(PaymentACallbackRequest $request)
    {
        try {
            $this->paymentService->callback($request->safe()->all());

            return response()
                ->json([
                    'success' => true,
                ]);
        } catch (BaseException $baseException) {
            return response()
                ->json([
                    'error' => $baseException->getMessage(),
                    'error_code' => $baseException->getErrorCode(),
                ], $baseException->getCode());
        } catch (ModelNotFoundException $modelNotFoundException) {
            return response()
                ->json([
                    'error' => 'transaction not found',
                    'error_code' => 'NOT_FOUND',
                ], 404);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'error' => 'Unahendled exception',
                    'error_code' => 'UNHANDLED_EXCEPTION',
                ], 500);
        }
    }
}
