<?php

namespace App\Exceptions;

use Exception;

class BaseException extends Exception
{
    public function __construct(
        string $message = "",
        int $code = 500,
        protected string $errorCode)
    {
        parent::__construct($message, $code);
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
