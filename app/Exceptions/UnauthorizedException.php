<?php

namespace App\Exceptions;

use Exception;

class UnauthorizedException extends BaseException
{
    public function __construct()
    {
        parent::__construct("Unauthorized", 401, 'Unauthorized');
    }
}
