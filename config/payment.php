<?php

return [
    'a' => [
        'key' => env('MERCHANT_A_KEY'),
        'secret' => env('MERCHANT_A_SECRET'),
        'limit_transaction_per_day' => env('MERCHANT_A_LIMIT_PER_DAY', -1),
    ],
    'b' => [
        'key' => env('MERCHANT_B_KEY'),
        'secret' => env('MERCHANT_B_SECRET'),
        'limit_transaction_per_day' => env('MERCHANT_B_LIMIT_PER_DAY', -1),
    ],
];
