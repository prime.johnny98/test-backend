<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('merchant_type');
            $table->string('merchant_id');
            $table->string('external_id');
            $table->enum('status', \App\Services\AbstractPaymentService::getStatuses());
            $table->integer('amount');
            $table->integer('amount_paid');
            $table->timestamps();

            $table->unique('merchant_type', 'external_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
